package bo.com.cognos.springboot.entidades.exception;

public class ApplicationException extends RuntimeException {
	public ApplicationException(String mensaje) {
		super(mensaje);
	}
}
