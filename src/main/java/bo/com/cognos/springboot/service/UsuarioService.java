package bo.com.cognos.springboot.service;

import java.util.Arrays;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import bo.com.cognos.springboot.dao.RolRepository;
import bo.com.cognos.springboot.dao.UsuarioRepository;
import bo.com.cognos.springboot.entidades.Rol;
import bo.com.cognos.springboot.entidades.Usuario;

@Service
public class UsuarioService {

	@Autowired
	UsuarioRepository repository;
	
	@Autowired
	RolRepository rolRepository;
	
	public Usuario autenticar(String login, String password) {
		Usuario usuario = repository.findByLogin(login);
		if(usuario == null) {
			throw new UsernameNotFoundException("Nombre de usuario no encontrado");
		}
		/*if(!usuario.getPassword().equals(password)) {
			throw new BadCredentialsException("Contraseña incorrecta");
		}*/
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		if(!bCryptPasswordEncoder.matches(password, usuario.getPassword())) {
			throw new BadCredentialsException("Contraseña incorrecta");
		}
		return usuario;
	}
	
	@Transactional
	public void inicializar() {
		System.out.println("Ejecutando inicializar");
		if(rolRepository.count() == 0) {
			System.out.println("Creara datos base");
			Rol rolAdmin = new Rol(null, "ROLE_ADMIN");
			Rol rolUser = new Rol(null, "ROLE_USER");
			rolRepository.saveAndFlush(rolAdmin);
			rolRepository.saveAndFlush(rolUser);
			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
			Usuario usrAdmin = new Usuario(null, "admin", encoder.encode("admin"), new HashSet<>(Arrays.asList(rolAdmin)));
			Usuario usrUser = new Usuario(null, "user", encoder.encode("user"), new HashSet<>(Arrays.asList(rolUser)));
			Usuario usrRoot = new Usuario(null, "root", encoder.encode("root"), new HashSet<>(Arrays.asList(rolAdmin, rolUser)));
			repository.saveAndFlush(usrAdmin);
			repository.saveAndFlush(usrUser);
			repository.saveAndFlush(usrRoot);
		}
		else {
			System.out.println("La BD ya estaba inicializada");
		}
	}
	
}
