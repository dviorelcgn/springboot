package bo.com.cognos.springboot.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bo.com.cognos.springboot.entidades.Rol;

@Repository
public interface RolRepository extends JpaRepository<Rol, Integer> {
	
}
