package bo.com.cognos.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import bo.com.cognos.springboot.service.UsuarioService;

@Component
public class ConsoleListener implements CommandLineRunner {

	@Autowired
	UsuarioService usuarioService;
	
	@Override
	public void run(String... args) throws Exception {
		usuarioService.inicializar();
	}

}
