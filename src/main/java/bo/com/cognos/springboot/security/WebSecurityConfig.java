 package bo.com.cognos.springboot.security;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig 
			extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers("/rest/**").anonymous()
			.anyRequest().
	        hasAnyRole("USER", "ADMIN")
			.and()
			// Para Basic
			//.httpBasic();
			
			// Para formulario autogenerado:
//			.formLogin().permitAll()
//			.and().logout().permitAll();
		
			// Para formulario personalizado:
			.formLogin().loginPage("/login")
			.successHandler(getAuthenticationSuccessHandler())
		      .usernameParameter("user").passwordParameter("password")
		.permitAll().and().logout().permitAll();

	}

	
	@Bean
	public PasswordEncoder passwordEncoder() {
		Map<String, PasswordEncoder> encoders = new HashMap<>();
		encoders.put("noop", org.springframework.security.crypto.password.NoOpPasswordEncoder.getInstance());
		return new DelegatingPasswordEncoder("noop", encoders);
	}

	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
	  auth.inMemoryAuthentication()
	  	.withUser("admin").password("{noop}admin123").roles("ADMIN")
	  	.and().withUser("user").password("{noop}user123").roles("USER")
	  	.and().withUser("root").password("{noop}root123").roles("USER", "ADMIN");
	  
	  //auth.authenticationProvider(new CustomAuthenticationProvider());
	  auth.authenticationProvider(getAuthenticationProvider());
	  
	}
	
	@Bean
	public AuthenticationProvider getAuthenticationProvider() {
		return new CustomAuthenticationProvider();
	}
	
	@Bean
	public AuthenticationSuccessHandler getAuthenticationSuccessHandler() {
		return new CustomAuthenticationSuccessHandler();
	}

	
}
