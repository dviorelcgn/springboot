package bo.com.cognos.springboot.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		// TODO Auto-generated method stub
		User authUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		// Object obj =
		// SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//		if (obj instanceof User) {
//			User authUser = (User) obj;
		System.out.println("Usuario autenticado: " + authUser.getUsername());
		System.out.println("Con los siguientes roles: ");
//					for(GrantedAuthority rol: authUser.getAuthorities()) {
//						System.out.println(rol.getAuthority());
//					}
		authUser.getAuthorities().stream().forEach(System.out::println);
//		}
//		if (obj instanceof String) {
//			System.out.println("Usuario autenticado: " + obj);
//		}
		response.sendRedirect("estudiante");
	}

}
