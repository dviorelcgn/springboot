package bo.com.cognos.springboot.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import bo.com.cognos.springboot.entidades.Estudiante;
import bo.com.cognos.springboot.service.EstudianteService;



@RestController
@RequestMapping("/rest/estudiante")
public class EstudianteRestController {
	
	@Autowired
	EstudianteService estudianteService;
	
	@RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})	
	public List<Estudiante> listar(){
		return estudianteService.listar();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Estudiante obtener(@PathVariable("id") Integer id){
		return estudianteService.obtener(id);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public Estudiante crear(@RequestBody Estudiante estudiante) {
		estudianteService.agregar(estudiante);
		return estudiante;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Estudiante editar(@RequestBody Estudiante estudiante, @PathVariable("id") Integer id) {	
		estudiante.setId(id);
		estudianteService.editar(estudiante);
		return estudiante;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public String eliminar(@PathVariable("id") Integer id) {
		estudianteService.eliminar(id);
		return "OK";
	}

}
