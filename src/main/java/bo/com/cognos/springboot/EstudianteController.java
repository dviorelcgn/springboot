package bo.com.cognos.springboot;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import bo.com.cognos.springboot.entidades.Estudiante;
import bo.com.cognos.springboot.service.EstudianteService;



@Controller
@RequestMapping("/estudiante")
public class EstudianteController {
	
	@Autowired
	EstudianteService estudianteService;
	
	
	@GetMapping(value = {"", "/"})
	public String inicioPersona(Model model) {
		
		List<Estudiante> estudiantes = estudianteService.listar();
		model.addAttribute("estudiantes", estudiantes);
		model.addAttribute("editando", false);
		return "estudiante";
	}
	
	@GetMapping("/agregar")
	public String preparCrearPersona(Model model) {
		model.addAttribute("estudiante", new Estudiante());
		model.addAttribute("editando", true);
		return "estudiante";
	}
	
	@PostMapping("/guardar")
	public String guardarPersona(Model model, @ModelAttribute("estudiante")Estudiante estudiante, HttpServletRequest request) {
		if(request.getParameter("Cancelar") == null) {
			if(estudiante.getId() != null) {				
				estudianteService.editar(estudiante);
			}
			else {
				estudianteService.agregar(estudiante);
			}
		}
		model.addAttribute("editando", false);
		model.addAttribute("estudiantes", estudianteService.listar());
		return "redirect:/estudiante/";
	}
	
	@GetMapping("/editar/{id}")
	public String preparEditarPersona(Model model, @PathVariable Integer id) {
		Estudiante estudiante = estudianteService.obtener(id);
		model.addAttribute("estudiante", estudiante);
		model.addAttribute("editando", true);
		return "estudiante";
	}
	
	@RequestMapping(value = "/borrar", method = RequestMethod.POST)
	public String borrarPersona(Model model, @RequestParam("id")Integer id) {
		estudianteService.eliminar(id);
		model.addAttribute("estudiante", estudianteService.listar());
		model.addAttribute("editando", false);
		return "redirect:/estudiante/";
	}
}
